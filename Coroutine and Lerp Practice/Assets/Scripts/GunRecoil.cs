﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunRecoil : MonoBehaviour
{

    public ParticleSystem MuzzleFlash;


    private void OnMouseDown() {
        recoil();

    }
    
    private void recoil() {
        MuzzleFlash.Emit(2);
        StartCoroutine("sniper");
    }
    
    private IEnumerator sniper() {
        float t = 0;
        float duration = 0.05f;

        Vector3 start = transform.position;
        Vector3 end   =  -transform.forward;
    
        while (t < duration) {
            transform.position = Vector3.Lerp(start, end, t/duration);

    
            t += Time.deltaTime;

            yield return null;
        }

        if (duration == 0.05f) {
            
            yield return new WaitForSeconds(0.3f);

            transform.position = Vector3.Lerp(end, start, t/duration);

            yield return null;
        }
    }
}    