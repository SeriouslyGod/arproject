﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shakey : MonoBehaviour {

    public ParticleSystem leafFalling;

    private bool shaking = false;
    private float shakeAmt = 1f;

    private void OnMouseDown(){
        ShakeMe();
    }

    private void Update() {

        if (shaking == true) {
            Vector3 newPos = transform.position + Random.insideUnitSphere * (Time.deltaTime * shakeAmt);
            newPos.y = transform.position.y;

            transform.position = newPos;

            leafFalling.Emit(3);
        }
    }

    private void ShakeMe() {
        StartCoroutine("ShakeNow");
    }

    IEnumerator ShakeNow() {
        Vector3 originalPos = transform.position;

        if (shaking == false) {
            shaking = true;
        }

        yield return new WaitForSeconds(0.25f);

        shaking = false;
        transform.position = originalPos;
    }
}