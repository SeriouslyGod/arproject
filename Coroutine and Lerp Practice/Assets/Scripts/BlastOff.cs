﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlastOff : MonoBehaviour{
    public AnimationCurve anim;

    private void OnMouseDown() {
        TakeOff();
    }
    
    private void TakeOff() {
        StartCoroutine("Lift");
    }

    private IEnumerator Lift() {
        float t = 0;
        float duration = 5.0f;

        Vector3 current = transform.position;
        Vector3 end = new Vector3(0, 99, 0);
        
    
        while (t < duration) {
            transform.position = Vector3.Lerp(current, end, anim.Evaluate(t/duration));
    
            t += Time.deltaTime;
            yield return null;
        }
        
        if (duration == 5.0f) {
            Destroy(gameObject);
            Debug.Log("Destroyed");
        }
    }
}
