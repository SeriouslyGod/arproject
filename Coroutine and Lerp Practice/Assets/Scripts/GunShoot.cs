﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunShoot : MonoBehaviour {
    
    private bool recoil = false;

    
    private void OnMouseDown() {
        shoot();
    }

    // private void Update() {

    // }

    private void shoot() {
        StartCoroutine("sniper");
    }

    private IEnumerator sniper () {
        // Debug.Log("HELO");
    
        Vector3 originalPos = transform.position;

        if (recoil == false) {
            recoil = true;
        }

        if (recoil == true) {
            Vector3 newPos = transform.position + -transform.forward * Time.deltaTime * 20; 

            transform.position = newPos;
        }

        yield return new WaitForSeconds(0.1f);

        transform.position = originalPos;

    }

        // Shoot(transform.forward);
        
}

    // void Shoot(Vector3 _direction) {
    //     bullet.velocity = _direction * 99;
    // }

