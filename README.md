# ARProject



The client has requested that there are atleast 3 ojbects that can be taken out and interracted with in this Augmented Reality project.
We will have interactable objects in the UI at the bottom of the screen where they can be dragged and dropped into the plane where they can be interacted with.
There will be audio and particles to go with the animation of the objects.

The 3 objects that will be done are :

- Sniper
- Rocket Ship
- Tree


**Sniper**

- Shoot a bullet

- Recoil animation

- A gun sound



**Rocket Ship**

- Flys up into the sky

- Ignition flames animation

- Ignition sound



**Tree**

- A leaf falls off

- Leaf swaying down to the ground

- Rustling sound



**SCHEDULE**

27/3/2019 - Start the project

28/3/2019 - Make the tree asset

29/3/2019 - Make the textures of the tree and start on the scripts

30/3/2019 - Break

31/3/2019 - Break

1/4/2019 - Make the sniper and rocket asset and texture them.

2/4/2019 - Make scripts for the movement of all three assets.

3/4/2019 - Finish the movement scripts of the assets and start on particle systems.

4/4/2019 - still working on the particle systems and started on audio.

5/4/2019 - Finish particle systems and audio and started to test on the application.