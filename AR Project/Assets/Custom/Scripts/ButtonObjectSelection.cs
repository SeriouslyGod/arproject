﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using UnityEngine.EventSystems;


public class ButtonObjectSelection : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler {
    public class MaterialSet {
        public MaterialSet(Material _material) {
            material = _material;
            color = _material.GetColor("_Color");
        }

        public Material material;
        public Color color;
    }

    
    public Interactable interactableToSpawn;
    private LayerMask mask;

    private Vector3 initialPosition;
    private Vector3 spawnPosition;
    private Quaternion spawnRotation;
    private RaycastHit hit;
    private Button button;
    private List<MaterialSet> previewMaterialSets = new List<MaterialSet>();

    private void Awake() {
        button = GetComponent<Button>();
        mask = LayerMask.GetMask("Terrain");
        initialPosition = interactableToSpawn.transform.position;
        interactableToSpawn.gameObject.SetActive(false);

        PopulatePreviewMaterials();
    }

    private void Update() {
        UpdatePreviewMaterial();
    }

    public void OnPointerDown(PointerEventData eventData) {
        FlagObjectForPlacement();
    }

    public void OnDrag(PointerEventData eventData) {
        RaycastFindTerrain();
    }

    public void OnPointerUp(PointerEventData eventData) {
        if (hit.transform && DetectOverlap.overlaps.Count == 0) {
            SpawnCopy();
        }

        RestoreAvailability();
    }
    
    private void FlagObjectForPlacement() {
        SetObjectLayer(interactableToSpawn.transform, "Overlay");

        button.interactable = false;
        interactableToSpawn.gameObject.SetActive(true);

        MoveAlongOverlay();
    }
    
    private void RestoreAvailability() {
        RecolourMaterialsInHierarchy(Color.white);
        hit = new RaycastHit();

        SetObjectLayer(interactableToSpawn.transform, "Overlay");

        DetectOverlap.overlaps.Clear();

        button.interactable = true;
        interactableToSpawn.gameObject.SetActive(false);
        interactableToSpawn.transform.position = initialPosition;
    }
    
    private void SetObjectLayer(Transform _transform, string _layerName) {
        Transform[] transforms = _transform.GetComponentsInChildren<Transform>();
        foreach (Transform t in transforms) {
            t.gameObject.layer = LayerMask.NameToLayer(_layerName);
        }
    }
    
    private void RaycastFindTerrain() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 200, mask)) {
            MoveAlongTerrain();
        } else {
            MoveAlongOverlay();
        }
    }
    
    private void MoveAlongTerrain() {
        SetObjectLayer(interactableToSpawn.transform, "Default");

        spawnPosition = hit.point;
        interactableToSpawn.transform.position = spawnPosition;

        spawnRotation = Quaternion.FromToRotation(interactableToSpawn.transform.up, hit.normal) * interactableToSpawn.transform.rotation;
        interactableToSpawn.transform.rotation = spawnRotation;
    }
    
    private void MoveAlongOverlay() {
        SetObjectLayer(interactableToSpawn.transform, "Overlay");

        Vector3 pos = Input.mousePosition;
        pos.z = transform.forward.z - Camera.main.transform.forward.z + 20;

        interactableToSpawn.transform.position = Camera.main.ScreenToWorldPoint(pos);
        interactableToSpawn.transform.rotation = Camera.main.transform.rotation;
    }
    
    private void SpawnCopy() {
        Transform t = Instantiate(interactableToSpawn, hit.point, spawnRotation, CustomTrackableEventHandler.instance.transform).transform;
        t.localScale = interactableToSpawn.transform.localScale;
        SetObjectLayer(t, "Default");

        Destroy(t.GetComponent<DetectOverlap>());
    }
    
    // Create a separate list of materials we can mess with when toggling between green/red for free or overlapped geometry.
    private void PopulatePreviewMaterials() {
        Renderer[] renderers = interactableToSpawn.gameObject.GetComponentsInChildren<Renderer>();
 
        foreach (Renderer r in renderers) {
            foreach (Material m in r.materials) {
                previewMaterialSets.Add(new MaterialSet(m));
            }
        }
    }
 
    // Colour our materials red or green if our dragged object is above terrain, otherwise colour it white.
    private void UpdatePreviewMaterial() {
        if (previewMaterialSets == null || previewMaterialSets.Count <= 0) { return; }
 
        if (hit.transform) {
            RecolourMaterialsInHierarchy((DetectOverlap.overlaps.Count > 0) ? Color.red : Color.green);
        } else {
            RecolourMaterialsInHierarchy(Color.white);
        }
    }
 
    // Just a method to summarize what happens when recolouring our materials.
    private void RecolourMaterialsInHierarchy(Color _colour) {
        Renderer[] renderers = interactableToSpawn.GetComponentsInChildren<Renderer>();
        for (int i = 0; i < renderers.Length; i++) {
            renderers[i].material.SetColor("_Color", _colour == Color.white ? previewMaterialSets[i].color : _colour);
        }
    }
}
