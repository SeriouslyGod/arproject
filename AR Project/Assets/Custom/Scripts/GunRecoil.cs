﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunRecoil : Interactable
{

    public ParticleSystem muzzleFlash;

    public AudioSource audioSrc;
    
    protected override void OnMouseDown() {
        Respond();
    }

    protected override void Respond() {
        Recoil();
    }
    
    private void Recoil() {
        audioSrc.Play();
        muzzleFlash.Play();
        StartCoroutine("sniper");
    }
    
    private IEnumerator sniper() {
        float t = 0;
        float duration = 0.05f;

        Vector3 start = transform.position;
        Vector3 end = transform.position - transform.forward;

        
    
        while (t < duration) {
            transform.position = Vector3.Lerp(start, end, t/duration);
    
            t += Time.deltaTime;

            yield return null;
        }

        if (duration == 0.05f) {
            yield return new WaitForSeconds(0.3f);

            transform.position = Vector3.Lerp(end, start, t/duration);

            yield return null;
        }
    }
}    