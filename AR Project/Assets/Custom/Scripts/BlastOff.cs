﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlastOff : Interactable{
    public AnimationCurve anim;
    public ParticleSystem rocketFire;
    public AudioSource audioSrc;

    protected override void OnMouseDown()
    {
        Respond();
    }

    protected override void Respond()
    {
        TakeOff();
    }
    
    private void TakeOff() {
        audioSrc.Play();
        rocketFire.Play();
        StartCoroutine("Lift");
    }

    private IEnumerator Lift() {
        float t = 0;
        float duration = 5.0f;

        Vector3 current = transform.localPosition;
        Vector3 end = new Vector3(0, 99, 0);
        
    
        while (t < duration) {
            transform.localPosition = Vector3.Lerp(current, end, anim.Evaluate(t/duration));
    
            t += Time.deltaTime;
            yield return null;
        }
        
        if (duration == 5.0f) {
            Destroy(gameObject);
        }
    }

}
