﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shakey : Interactable {

    public ParticleSystem leafFalling;

    public AudioSource audioSrc;

    private bool shaking = false;
    private float shakeAmt = 1f;

    protected override void OnMouseDown(){
        Respond();
    }

    protected override void Respond() {
        Shake();
    }
    private void Update() {

        if (shaking == true) {
            Vector3 newPos = transform.position + Random.insideUnitSphere * (Time.deltaTime * shakeAmt);
            newPos.y = transform.position.y;

            transform.position = newPos;

            leafFalling.Emit(3);
        }
    }


    private void Shake() {
        audioSrc.Play();
        StartCoroutine("ShakeNow");
    }

    IEnumerator ShakeNow() {
        Vector3 originalPos = transform.position;

        if (shaking == false) {
            shaking = true;
        }

        yield return new WaitForSeconds(0.25f);

        shaking = false;
        transform.position = originalPos;
    }
}